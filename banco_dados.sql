-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 22-Jan-2018 às 20:12
-- Versão do servidor: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carrinho_compras`
--
CREATE DATABASE IF NOT EXISTS `carrinho_compras` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `carrinho_compras`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `caracteristicas`
--

CREATE TABLE `caracteristicas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `caracteristicas`
--

INSERT INTO `caracteristicas` (`id`, `nome`, `created_at`, `updated_at`) VALUES
(1, 'Altura', '2018-01-16 18:39:43', '2018-01-16 18:45:45'),
(2, 'Largura', '2018-01-16 18:40:41', '2018-01-16 18:46:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `caracteristica_produtos`
--

CREATE TABLE `caracteristica_produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `caracteristicas_id` int(10) UNSIGNED NOT NULL,
  `produtos_id` int(10) UNSIGNED NOT NULL,
  `valor` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `caracteristica_produtos`
--

INSERT INTO `caracteristica_produtos` (`id`, `caracteristicas_id`, `produtos_id`, `valor`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, '2018-01-22 21:54:55', '2018-01-22 22:52:53'),
(2, 2, 1, 12, '2018-01-22 17:51:55', '2018-01-22 20:54:53'),
(3, 1, 2, 12, '2018-01-22 21:53:55', '2018-01-22 02:50:50'),
(4, 2, 2, 13, '2018-01-22 16:44:40', '2018-01-22 02:42:39'),
(5, 1, 3, 13, '2018-01-22 19:43:50', '2018-01-22 16:44:45'),
(6, 2, 3, 13, '2018-01-22 02:00:50', '2018-01-22 02:00:55'),
(7, 1, 4, 14, '2018-01-22 22:47:50', '2018-01-22 22:53:54'),
(8, 2, 4, 14, '2018-01-23 00:55:58', '2018-01-22 22:52:55'),
(9, 1, 5, 15, '2018-01-22 19:55:55', '2018-01-22 12:41:12'),
(10, 2, 5, 15, '2018-01-22 11:21:14', '2018-01-22 13:28:20'),
(11, 1, 6, 16, '2018-01-22 10:14:12', '2018-01-22 02:14:12'),
(12, 2, 6, 16, '2018-01-22 10:25:27', '2018-01-22 09:20:23'),
(13, 1, 7, 17, '2018-01-22 08:26:30', NULL),
(14, 2, 7, 17, '2018-01-22 12:32:35', '2018-01-22 11:32:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nome`, `created_at`, `updated_at`) VALUES
(1, 'Decoração', '2018-01-16 02:00:00', '2018-01-16 02:00:00'),
(2, 'Eletro', '2018-01-16 17:43:00', '2018-01-16 17:45:00'),
(3, 'Móveis', '2018-01-16 18:46:00', '2018-01-16 17:46:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_produtos`
--

CREATE TABLE `categoria_produtos` (
  `categorias_id` int(10) UNSIGNED NOT NULL,
  `produtos_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `categoria_produtos`
--

INSERT INTO `categoria_produtos` (`categorias_id`, `produtos_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-01-17 02:51:50', '2018-01-17 16:40:43'),
(1, 4, '2018-01-17 21:00:00', '2018-01-17 02:00:53'),
(1, 5, '2018-01-22 22:54:53', '2018-01-22 02:52:52'),
(2, 1, '2018-01-17 02:00:46', '2018-01-17 02:00:57'),
(2, 3, '2018-01-17 02:00:51', '2018-01-17 02:00:54'),
(2, 6, '2018-01-22 18:46:50', '2018-01-22 22:50:51'),
(3, 2, '2018-01-17 02:00:38', '2018-01-17 02:00:51'),
(3, 7, '2018-01-22 19:46:49', '2018-01-22 21:49:52');

-- --------------------------------------------------------

--
-- Estrutura da tabela `enderecos`
--

CREATE TABLE `enderecos` (
  `id` int(10) UNSIGNED NOT NULL,
  `logradouro` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `cidade` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cep` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `enderecos`
--

INSERT INTO `enderecos` (`id`, `logradouro`, `numero`, `cidade`, `cep`, `bairro`, `estado`, `created_at`, `updated_at`) VALUES
(10, '123', 123, '123', '123', '123', '12', '2018-01-22 07:25:46', '2018-01-22 07:25:46'),
(11, '123', 33, '3', '3', '3', '3', '2018-01-22 07:27:51', '2018-01-22 07:27:51'),
(12, '123', 33, '3', '3', '3', '3', '2018-01-22 07:47:15', '2018-01-22 07:47:15'),
(13, '99', 99, '99', '99', '99', '99', '2018-01-22 07:48:00', '2018-01-22 07:48:00'),
(14, '99', 99, '99', '99', '99', '99', '2018-01-22 07:50:40', '2018-01-22 07:50:40'),
(15, '99', 99, '99', '99', '99', '99', '2018-01-22 07:50:52', '2018-01-22 07:50:52'),
(16, '99', 99, '99', '99', '99', '99', '2018-01-22 07:51:42', '2018-01-22 07:51:42'),
(17, '99', 99, '99', '99', '99', '99', '2018-01-22 07:52:01', '2018-01-22 07:52:01'),
(18, '99', 99, '99', '99', '99', '99', '2018-01-22 07:59:13', '2018-01-22 07:59:13'),
(19, '99', 99, '99', '99', '99', '99', '2018-01-22 07:59:27', '2018-01-22 07:59:27'),
(20, '1', 1, '1', '1', '1', '1', '2018-01-22 08:00:59', '2018-01-22 08:00:59'),
(21, 'rua de cima', 123, 'de cima', '1230400', 'de cima', 'sp', '2018-01-22 18:00:10', '2018-01-22 18:00:10'),
(22, '123', 123, '123', '123', '123', '12', '2018-01-22 19:04:55', '2018-01-22 19:04:55'),
(23, '432', 432, '432', '234', '432', '43', '2018-01-22 19:05:19', '2018-01-22 19:05:19'),
(24, '456654', 456456, '456456', '456456', '456', '45', '2018-01-22 19:05:37', '2018-01-22 19:05:37'),
(25, '6465', 654, '64654', '654', '654', '65', '2018-01-22 19:05:58', '2018-01-22 19:05:58'),
(26, 'mhg', 34, '345', '345', '345', '34', '2018-01-22 21:07:09', '2018-01-22 21:07:09'),
(27, '1', 11, '1', '1', '1', '1', '2018-01-22 22:44:27', '2018-01-22 22:44:27'),
(28, '323', 312, '312', '123', '312', '12', '2018-01-23 00:08:51', '2018-01-23 00:08:51'),
(29, '123', 123, '123', '123', '123', '12', '2018-01-23 00:09:33', '2018-01-23 00:09:33'),
(30, '89498', 984984, '984', '984', '984', '98', '2018-01-23 00:10:11', '2018-01-23 00:10:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_01_16_035231_create_enderecos_table', 1),
(2, '2018_01_16_035441_create_usuarios_table', 1),
(3, '2018_01_16_035533_create_categorias_table', 1),
(4, '2018_01_16_035709_create_pedidos_table', 1),
(5, '2018_01_16_035821_create_produtos_table', 1),
(6, '2018_01_16_035926_create_caracteristicas_table', 1),
(8, '2018_01_16_040057_create_pedidos_produtos_table', 1),
(10, '2018_01_16_040012_create_categoria_produtos_table', 2),
(11, '2018_01_22_193949_create_caracteristica_produtos_table', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `usuarios_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `pedidos`
--

INSERT INTO `pedidos` (`id`, `usuarios_id`, `created_at`, `updated_at`) VALUES
(8, 14, '2018-01-22 07:52:01', '2018-01-22 07:52:01'),
(9, 15, '2018-01-22 07:59:13', '2018-01-22 07:59:13'),
(10, 16, '2018-01-22 07:59:27', '2018-01-22 07:59:27'),
(11, 17, '2018-01-22 08:00:59', '2018-01-22 08:00:59'),
(12, 18, '2018-01-22 18:00:10', '2018-01-22 18:00:10'),
(13, 19, '2018-01-22 19:04:55', '2018-01-22 19:04:55'),
(14, 20, '2018-01-22 19:05:19', '2018-01-22 19:05:19'),
(15, 21, '2018-01-22 19:05:37', '2018-01-22 19:05:37'),
(16, 22, '2018-01-22 19:05:58', '2018-01-22 19:05:58'),
(17, 23, '2018-01-22 21:07:09', '2018-01-22 21:07:09'),
(18, 25, '2018-01-23 00:08:51', '2018-01-23 00:08:51'),
(19, 26, '2018-01-23 00:09:34', '2018-01-23 00:09:34'),
(20, 27, '2018-01-23 00:10:11', '2018-01-23 00:10:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_produtos`
--

CREATE TABLE `pedido_produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `pedidos_id` int(10) UNSIGNED NOT NULL,
  `produtos_id` int(10) UNSIGNED NOT NULL,
  `quantidade` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `pedido_produtos`
--

INSERT INTO `pedido_produtos` (`id`, `pedidos_id`, `produtos_id`, `quantidade`, `created_at`, `updated_at`) VALUES
(2, 8, 1, 4, '2018-01-22 07:52:01', '2018-01-22 07:52:01'),
(3, 9, 1, 4, '2018-01-22 07:59:13', '2018-01-22 07:59:13'),
(4, 10, 1, 4, '2018-01-22 07:59:27', '2018-01-22 07:59:27'),
(5, 11, 1, 4, '2018-01-22 08:00:59', '2018-01-22 08:00:59'),
(6, 12, 2, 3, '2018-01-22 18:00:10', '2018-01-22 18:00:10'),
(7, 12, 3, 3, '2018-01-22 18:00:10', '2018-01-22 18:00:10'),
(8, 13, 4, 5, '2018-01-22 19:04:55', '2018-01-22 19:04:55'),
(9, 13, 1, 2, '2018-01-22 19:04:55', '2018-01-22 19:04:55'),
(10, 13, 2, 2, '2018-01-22 19:04:55', '2018-01-22 19:04:55'),
(11, 14, 2, 7, '2018-01-22 19:05:20', '2018-01-22 19:05:20'),
(12, 15, 1, 2, '2018-01-22 19:05:37', '2018-01-22 19:05:37'),
(13, 16, 2, 6, '2018-01-22 19:05:58', '2018-01-22 19:05:58'),
(14, 17, 5, 2, '2018-01-22 21:07:09', '2018-01-22 21:07:09'),
(15, 18, 3, 1, '2018-01-23 00:08:51', '2018-01-23 00:08:51'),
(16, 19, 2, 1, '2018-01-23 00:09:34', '2018-01-23 00:09:34'),
(17, 20, 3, 1, '2018-01-23 00:10:11', '2018-01-23 00:10:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preco` decimal(6,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `descricao`, `imagem`, `preco`, `created_at`, `updated_at`) VALUES
(1, 'Cadeira de Escritório Secretária Java Preta', 'A Cadeira de Escritório Secretária Java é uma peça básica e essencial para qualquer ambiente coorporativo. Produzida na cor preta, proporciona um visual arrojado e elegante ao espaço. ', 'Cadeira_de_Escritório Secretária_Java_Preta.jpg', '249.99', '2018-01-16 18:43:30', '2018-01-16 18:45:40'),
(2, 'Cadeira de Escritório Diretor Oslo Branca', 'Precisando de uma cadeira confortável para o seu escritório? Que tal a Cadeira de Escritório Oslo? Ela é ideal para proporcionar charme e bom gosto para o ambiente, deixando você muito mais à vontade para realizar o seu trabalho.', 'Cadeira_de_Escritório_Diretor_Oslo_Branca.jpg', '289.99', '2018-01-16 15:40:35', '2018-01-16 20:51:47'),
(3, 'Almofada Shirras Suede Bege', 'Que não gosta de conforto? Seja na hora de sentar, encostar ou apenas para apoiar o braço, a Almofada Shirras é a peça perfeita! Moderna, ela possui os detalhes em botone que dão todo o charme ao produto. ', 'Almofada_Shirras_Suede_Bege.jpg', '45.99', '2018-01-16 15:38:45', '2018-01-16 17:45:50'),
(4, 'Moedor de Café Cadence Di Grano 150W MDR302 - 110V', 'Moedor de Café Cadence Di Grano 150W MDR302 O Moedor de Café Di Grano, é ideal para pessoas que apreciam e degustam o café, pois ao moer o café minutos antes do preparo, você mantém as propriedades do grão, o que realça o aroma e deixa um sabor incrível!', 'Moedor_de_Café_Cadence_Di_Grano_150W_MDR302_-_110V.jpg', '195.00', '2018-01-16 20:46:49', '2018-01-16 19:41:39'),
(5, 'Cadeira de Escritório Secretária Java Marrom', 'A Cadeira de Escritório Secretária Java é uma peça básica e essencial para qualquer ambiente coorporativo. Produzida na cor preta, proporciona um visual arrojado e elegante ao espaço. ', 'Cadeira_de_Escritório Secretária_Java_Preta.jpg', '299.51', '2018-01-25 19:53:57', '2018-01-22 22:54:52'),
(6, 'Cadeira de Escritório Diretor Oslo Azul', 'Precisando de uma cadeira confortável para o seu escritório? Que tal a Cadeira de Escritório Oslo? Ela é ideal para proporcionar charme e bom gosto para o ambiente, deixando você muito mais à vontade para realizar o seu trabalho.', 'Cadeira_de_Escritório_Diretor_Oslo_Branca.jpg', '250.00', '2018-01-22 19:46:49', '2018-01-22 23:51:50'),
(7, 'Moedor de Café Cadence Di Grano 150W MDR302 - 220V', 'Moedor de Café Cadence Di Grano 150W MDR302 O Moedor de Café Di Grano, é ideal para pessoas que apreciam e degustam o café, pois ao moer o café minutos antes do preparo, você mantém as propriedades do grão, o que realça o aroma e deixa um sabor incrível!', 'Moedor_de_Café_Cadence_Di_Grano_150W_MDR302_-_110V.jpg', '500.00', '2018-01-22 18:49:52', '2018-01-22 21:49:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enderecos_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `cpf`, `email`, `enderecos_id`, `created_at`, `updated_at`) VALUES
(7, '123', '1233213123123', '112332123@q24e.com', 10, '2018-01-22 07:25:46', '2018-01-22 07:25:46'),
(8, '777', '777', '77@11.com', 11, '2018-01-22 07:27:51', '2018-01-22 07:27:51'),
(9, '777', '77712', '771@11.com', 12, '2018-01-22 07:47:15', '2018-01-22 07:47:15'),
(10, '99', '999', '99@43.com', 13, '2018-01-22 07:48:00', '2018-01-22 07:48:00'),
(12, '99', '999321', '99312@43.com', 15, '2018-01-22 07:50:52', '2018-01-22 07:50:52'),
(13, '99', '999321312', '9931eqw2@43.com', 16, '2018-01-22 07:51:42', '2018-01-22 07:51:42'),
(14, '99', '9399321312', '39931eqw2@43.com', 17, '2018-01-22 07:52:01', '2018-01-22 07:52:01'),
(15, '99', '9399ui321312', '39931ikleqw2@43.com', 18, '2018-01-22 07:59:13', '2018-01-22 07:59:13'),
(16, '99', '9399ui3213123', '393931ikleqw2@43.com', 19, '2018-01-22 07:59:27', '2018-01-22 07:59:27'),
(17, '1', '1', '1@1.c', 20, '2018-01-22 08:00:59', '2018-01-22 08:00:59'),
(18, 'gabriel', '123', '123@555', 21, '2018-01-22 18:00:10', '2018-01-22 18:00:10'),
(19, 'amanda', '321', '321@321', 22, '2018-01-22 19:04:55', '2018-01-22 19:04:55'),
(20, 'sdasd', '312312', '432@233', 23, '2018-01-22 19:05:19', '2018-01-22 19:05:19'),
(21, '56564', '45656456456', '445645664@5645645656', 24, '2018-01-22 19:05:37', '2018-01-22 19:05:37'),
(22, '95645646546546', '64654654', '654646@213', 25, '2018-01-22 19:05:58', '2018-01-22 19:05:58'),
(23, 'mm', 'mm', 'm@m', 26, '2018-01-22 21:07:09', '2018-01-22 21:07:09'),
(25, '123', '12344444', '33@3232323', 28, '2018-01-23 00:08:51', '2018-01-23 00:08:51'),
(26, '33', 'fgsduyfga', 'fydgswyufgs@uidfshifs', 29, '2018-01-23 00:09:34', '2018-01-23 00:09:34'),
(27, 'dfvbdfgdfg', '68548964894', '894894@98489489489', 30, '2018-01-23 00:10:11', '2018-01-23 00:10:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `caracteristicas`
--
ALTER TABLE `caracteristicas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `caracteristica_produtos`
--
ALTER TABLE `caracteristica_produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `caracteristica_produtos_caracteristicas_id_foreign` (`caracteristicas_id`),
  ADD KEY `caracteristica_produtos_produtos_id_foreign` (`produtos_id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categoria_produtos`
--
ALTER TABLE `categoria_produtos`
  ADD PRIMARY KEY (`categorias_id`,`produtos_id`),
  ADD KEY `categoria_produtos_produtos_id_foreign` (`produtos_id`);

--
-- Indexes for table `enderecos`
--
ALTER TABLE `enderecos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedidos_usuarios_id_foreign` (`usuarios_id`);

--
-- Indexes for table `pedido_produtos`
--
ALTER TABLE `pedido_produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedidos_produtos_pedidos_id_foreign` (`pedidos_id`),
  ADD KEY `pedidos_produtos_produtos_id_foreign` (`produtos_id`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuarios_cpf_unique` (`cpf`),
  ADD UNIQUE KEY `usuarios_email_unique` (`email`),
  ADD KEY `usuarios_enderecos_id_foreign` (`enderecos_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `caracteristicas`
--
ALTER TABLE `caracteristicas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `caracteristica_produtos`
--
ALTER TABLE `caracteristica_produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `enderecos`
--
ALTER TABLE `enderecos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pedido_produtos`
--
ALTER TABLE `pedido_produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `caracteristica_produtos`
--
ALTER TABLE `caracteristica_produtos`
  ADD CONSTRAINT `caracteristica_produtos_caracteristicas_id_foreign` FOREIGN KEY (`caracteristicas_id`) REFERENCES `caracteristicas` (`id`),
  ADD CONSTRAINT `caracteristica_produtos_produtos_id_foreign` FOREIGN KEY (`produtos_id`) REFERENCES `produtos` (`id`);

--
-- Limitadores para a tabela `categoria_produtos`
--
ALTER TABLE `categoria_produtos`
  ADD CONSTRAINT `categoria_produtos_categorias_id_foreign` FOREIGN KEY (`categorias_id`) REFERENCES `categorias` (`id`),
  ADD CONSTRAINT `categoria_produtos_produtos_id_foreign` FOREIGN KEY (`produtos_id`) REFERENCES `produtos` (`id`);

--
-- Limitadores para a tabela `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_usuarios_id_foreign` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`);

--
-- Limitadores para a tabela `pedido_produtos`
--
ALTER TABLE `pedido_produtos`
  ADD CONSTRAINT `pedidos_produtos_pedidos_id_foreign` FOREIGN KEY (`pedidos_id`) REFERENCES `pedidos` (`id`),
  ADD CONSTRAINT `pedidos_produtos_produtos_id_foreign` FOREIGN KEY (`produtos_id`) REFERENCES `produtos` (`id`);

--
-- Limitadores para a tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_enderecos_id_foreign` FOREIGN KEY (`enderecos_id`) REFERENCES `enderecos` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
