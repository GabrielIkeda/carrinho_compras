<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaracteristicaProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('caracteristica_produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caracteristicas_id')->unsigned();
            $table->integer('produtos_id')->unsigned();
            $table->integer('valor');
            $table->timestamps();

            $table->foreign('caracteristicas_id')->references('id')->on('caracteristicas');
            $table->foreign('produtos_id')->references('id')->on('produtos');
        });
    }

    public function down()
    {
        //
    }
}
