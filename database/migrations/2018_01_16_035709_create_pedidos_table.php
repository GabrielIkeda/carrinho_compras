<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuarios_id')->unsigned();
            $table->timestamps();

            $table->foreign('usuarios_id')->references('id')->on('usuarios');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
