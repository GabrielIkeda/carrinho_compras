<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriaProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('categoria_produtos', function (Blueprint $table) {
            $table->integer('categorias_id')->unsigned();
            $table->integer('produtos_id')->unsigned();
            $table->timestamps();

            $table->primary(['categorias_id', 'produtos_id']);

            $table->foreign('categorias_id')->references('id')->on('categorias');
            $table->foreign('produtos_id')->references('id')->on('produtos');
        });
    }

    public function down()
    {
        Schema::DropIfExists('categoria_produtos');
    }
}
