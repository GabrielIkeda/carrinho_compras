<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('pedidos_produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pedidos_id')->unsigned();
            $table->integer('produtos_id')->unsigned();
            $table->integer('quantidade');
            $table->timestamps();

            $table->foreign('pedidos_id')->references('id')->on('pedidos');
            $table->foreign('produtos_id')->references('id')->on('produtos');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pedidos_produtos');
    }
}
