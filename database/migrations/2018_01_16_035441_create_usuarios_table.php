<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('cpf')->unique();
            $table->string('email')->unique();
            $table->integer('enderecos_id')->unsigned();
            $table->timestamps();

            $table->foreign('enderecos_id')->references('id')->on('enderecos');
        });
    }

    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
