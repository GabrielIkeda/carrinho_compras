<?php

if (env('APP_ENV') === 'production') {
    URL::forceSchema('https');
}

Route::get('/', 'HomeController@index')->name('index');
Route::post('/categoria', 'HomeController@categoria')->name('categoria');

Route::get('/produto/{id}', 'ProdutoController@index')->name('produto');

Route::post('/adicionar_produto/{id}', 'ProdutoController@adicionar_produto')->name('adicionar_produto');
Route::post('/pesquisa_produto', 'ProdutoController@pesquisa_produto')->name('pesquisa_produto');

Route::get('/remove_produto/{id}', 'CarrinhoController@remover_produto')->name('remover_produto');
Route::get('/carrinho_compras', 'CarrinhoController@index')->name('carrinho_compras');
Route::post('/atualiza_qtd/{id}', 'CarrinhoController@atualiza_qtd')->name('atualiza_qtd');

Route::get('/checkout', 'CheckoutController@index')->name('checkout');
Route::post('/registrar', 'CheckoutController@registrar')->name('registrar');

Route::get('/pedido', 'PedidoController@index')->name('pedido');
