@extends('templates.template_carrinho')

@section('content')
    @include('notificacao')
    <div class="row">
        <div class="col-lg-2">
            <form action="{{ route('categoria') }}" method="post">
                {{csrf_field()}}
                <div class="panel panel-default">
                    <div class="panel-body">
                        @foreach ($categorias as $key => $value)
                            <div class="checkbox">
                                @if (isset($filtro))
                                    @if (isset($filtro[$key]))
                                        @if ($filtro[$key] == $value->nome)
                                            <label><input type="checkbox" name="{{$value->id}}" value="{{$value->nome}}" onChange="this.form.submit()" checked>{{$value->nome}}</label>
                                        @endif
                                    @else
                                        <label><input type="checkbox" name="{{$value->id}}" value="{{$value->nome}}" onChange="this.form.submit()">{{$value->nome}}</label>
                                    @endif
                                @else
                                    <label><input type="checkbox" name="{{$value->id}}" value="{{$value->nome}}" onChange="this.form.submit()">{{$value->nome}}</label>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-10">
            @if (isset($produtos_exibicao))
                @foreach (array_chunk($produtos_exibicao, 3) as $produtoChunk)
                    <div class="row">
                        @foreach ($produtoChunk as $value)
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <a class="produtos_link" href="{{ route('produto', $value->id) }}" title="{{$value->nome}}">
                                    <div class="thumbnail produtos_div">
                                        <img class="img-responsive" src="img/produtos/thumbnail/{{$value->imagem}}" alt="{{$value->nome}}">
                                        <div class="caption">
                                            <h4>{{$value->nome}}</h4>
                                            <h4>R${{number_format($value->preco, 2, ',', '.')}}</h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @else
                @foreach (array_chunk($produtos->getCollection()->all(), 3) as $produtoChunk)
                    <div class="row">
                        @foreach ($produtoChunk as $value)
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <a class="produtos_link" href="{{ route('produto', $value->id) }}" title="{{$value->nome}}">
                                    <div class="thumbnail produtos_div">
                                        <img class="img-responsive" src="img/produtos/thumbnail/{{$value->imagem}}" alt="{{$value->nome}}">
                                        <div class="caption">
                                            <h4>{{$value->nome}}</h4>
                                            <h4>R${{number_format($value->preco, 2, ',', '.')}}</h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    {{$produtos->links()}}

@endsection
