@extends('templates.template_carrinho')

@section('content')

    @include('notificacao')

    <div class="row">
        <div class="col-lg-offset-3 col-lg-6">
            @if (Cache::has('carrinho'))
                <h1>Checkout</h1>
                <h3>Total: R${{number_format(Cache::get('carrinho')->total_preco, 2, ',','.')}}</h3>
                <form action="{{route('registrar')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" name="nome" id="nome" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="cpf">CPF</label>
                        <input type="text" name="cpf" id="cpf" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" name="email" id="email" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="logradouro">Logradouro</label>
                        <input type="text" name="logradouro" id="logradouro" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="numero">Número</label>
                        <input type="number" name="numero" id="numero" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="cep">CEP</label>
                        <input type="text" name="cep" id="cep" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="bairro">Bairro</label>
                        <input type="text" name="bairro" id="bairro" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="cidade">Cidade</label>
                        <input type="text" name="cidade" id="cidade" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="estado">Estado</label>
                        <input type="text" name="estado" id="estado" class="form-control" maxlength="2" required>
                    </div>
                    <button type="submit" class="btn btn-default">Registrar</button>
                </form>
            @else
                <h1>Erro! Carrinho vazio :(</h1>
            @endif
        </div>
    </div>

@endsection
