<link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.css') }}">
<script src="{{ asset('js/toastr.min.js') }}"></script>

<script>

    @if(Session::has('notification'))
        var type = "{{ Session::get('notification.alert-type', 'info') }}";
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "2500",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        var msg = "{{ Session::get('notification.message') }}";
        switch(type){
            case 'info':
                toastr.info(msg);
                break;

            case 'warning':
                toastr.warning(msg);
                break;

            case 'success':
                toastr.success(msg);
                {{ Session::forget('notification')}}
                break;

            case 'error':
                toastr.error(msg);
                {{ Session::forget('notification')}}
                break;
            }
    @endif
</script>
