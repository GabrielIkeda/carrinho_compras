@extends('templates.template_carrinho')

@section('content')

    <h1>Pedidos</h1>
    <br>
    @foreach ($pedidos as $key => $value)
        <div class="row">

            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-lg-3">
                        <h4>Cliente: {{$value->usuario->nome}}</h4>
                    </div>
                    <div class="col-lg-5">
                        <h4>Produtos:</h4>
                        <ul>
                            @foreach ($value->produto as $k => $v)
                                <li>{{$v->nome}} - x{{$value->pedido_produto[$k]->quantidade}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-lg-4">
                        <h4 class="pull-right">Total: {{$total[$key]}}</h4>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    @endforeach

    {{$pedidos->links()}}

@endsection
