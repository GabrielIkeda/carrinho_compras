@extends('templates.template_carrinho')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1>{{$produto->nome}}</h1>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <img class="img-responsive" src="../img/produtos/unitario/{{$produto->imagem}}">
            <hr>
            <p>{{$produto->descricao}}</p>
        </div>
        <div class="col-lg-4">
            <form action="{{ route('adicionar_produto', ['id' => $produto->id]) }}" method="post">
                {{csrf_field()}}
                <h3>Preço:</h3>
                <h2>R${{number_format($produto->preco, 2, ',', '.')}}</h2>
                <button class="btn btn-success" type="submit" name="button">Adicionar ao carrinho</button>
                <br><br>
                <div class="form-group">
                    <label for="quantidade">Quantidade</label>
                    <select class="form-control" name="quantidade">
                        @if (isset($limite))
                            @if ($limite == 10)
                                <option selected>Limite excedido!</option>
                            @else
                                @for ($i = 1; $i < $limite+1; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            @endif
                        @else
                            @for ($i = 1; $i < 11; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        @endif
                    </select>
                </div>
                <br>
            </form>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Categorias</h3>
                </div>
                <div class="panel-body">
                    @foreach ($produto->categoria as $key => $value)
                        {{$value->nome}}
                        <br>
                    @endforeach
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Características</h3>
                </div>
                <div class="panel-body">
                    @foreach ($caracteristicas as $key => $value)
                        {{$key}} - {{$value}}
                        <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
