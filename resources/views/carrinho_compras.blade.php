@extends('templates.template_carrinho')

@section('content')

    @if (Cache::has('carrinho'))
        @if (empty($produtos))
            <div class="row">
                <div class="col-lg-12">
                    <h1>Nenhum item adicionado ao carrinho</h1>
                </div>
            </div>
        @else
            <div class="row">
            <div class="col-lg-8">
                <hr>
                @foreach ($produtos as $key => $produto)
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <img class="img-responsive img_produto_carrinho center-block"
                            src="img/produtos/thumbnail/{{$produto['item']['imagem']}}" alt="{{$produto['item']['nome']}}">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <strong><p class="text-center">{{$produto['item']['nome']}}</p></strong>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="text-center">R${{number_format($produto['item']['preco'], 2, ',', '.')}}</p>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 quantidade_carrinho">
                            <form action="{{ route('atualiza_qtd', ['id' => $produto['item']['id']]) }}" method="post">
                                {{csrf_field()}}
                                <select class="form-control" name="quantidade" id="quantidade" onchange="this.form.submit()" >
                                    @for ($i = 1; $i < 11; $i++)
                                        @if ($i == $produto['qtd'])
                                            <option value="{{$i}}" selected>{{$i}}</option>
                                        @else
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endif

                                    @endfor
                                </select>
                            </form>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <p class="text-center">R${{number_format($produto['preco'], 2, ',', '.')}}</p>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1">
                            <a href="{{ route('remover_produto', ['id' => $produto['item']['id']]) }}" title="Remover item" class="center-block"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        </div>
                    </div>
                    <hr>
                @endforeach
            </div>
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="text-center">Total</h2>
                        <h4 class="text-center">R${{number_format($total_preco, 2, ',', '.')}}</h4>
                        <button type="button" class="btn btn-success btn-lg center-block">
                            <a href="{{ route('checkout') }}">Finalizar</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @endif
    @else
        <div class="row">
            <div class="col-lg-12">
                <h1>Nenhum item adicionado ao carrinho</h1>
            </div>
        </div>
    @endif

@endsection
