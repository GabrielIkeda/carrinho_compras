<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE-edge" />

        <title>Carrinho de compras</title>

        <!-- Adiciona a viewport-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- Adiciona CSS Bootstrep -->
        <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">

        <!-- Adiciona CSS personalizado -->
        <link rel="stylesheet" href="{{url('css/estilo.css')}}">

        {{-- Adiciona JavaScript JQuery --}}
        <script type="text/javascript" src="{{asset('js/jquery-3.2.1.min.js')}}"></script>

        {{-- Adiciona JavaScript Bootstrap --}}
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

    </head>

    <body>

        <header>
            <!-- COMEÇO MENU -->
            <nav class="navbar navbar-default navbar-static-top">

                <div class="container">

                    <!-- LOGO / BOTÃO SMARTPHONE -->

                    <div class="navbar-header">

                        <button type="button" id="botao-menu" class="navbar-toggle collapsed"
                                data-toggle="collapse" data-target="#barra-navegacao">

                            <span class="sr-only">Alternar Menu</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>

                        </button>

                        <a href="/" class="navbar-brand">Carrinho</a>

                    </div>

                    <!-- FIM LOGO / BOTÃO SMARTPHONE -->

                    <!-- COMEÇO ITENS MENU -->

                    <div id="barra-navegacao" class="collapse navbar-collapse">

                        <ul class="nav navbar-nav">
                            <li><a href="/">Produtos</a></li>
                            <li><a href="{{route('pedido')}}">Pedidos</a></li>
                        </ul>

                        <form class="navbar-form navbar-left" action="{{ route('pesquisa_produto') }}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" class="form-control" name="pesquisa">
                            </div>
                            <button type="submit" class="btn btn-default">Procurar</button>
                        </form>

                        <ul class="nav navbar-nav navbar-right">

                            <li>

                                <a href="/carrinho_compras">
                                    <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
                                    <span class="badge">{{ Cache::has('carrinho') ? Cache::get('carrinho')->total_qtd : '' }}</span>
                                </a>

                            </li>

                        </ul>

                    </div>

                    <!-- FIM ITENS MENU -->

                </div>

            </nav>

            <!-- FIM MENU -->

        </header>

        <div class="container">

            @yield('content')

        </div>

        <footer>

            <div class="container">

                <div class="row">

                    <div class="col-md-offset-3 col-md-6">

                        <p align="center">

                            © 2018 Carrinho de compras - Todos os direitos reservados

                        </p>

                    </div>

                </div>

            </div>

        </footer>

    </body>

</html>
