@extends('templates.template_carrinho')

@section('content')

    <h2>Resultado para: {{$pesquisa}}</h2>
    <br>
    @foreach ($resultado as $value)
        <a class="produtos_link" href="{{ route('produto', $value->id) }}" title="{{$value->nome}}">
            <div class="row">
                <div class="col-lg-5 col-md-5">
                    <img class="img-responsive center-block" src="img/produtos/thumbnail/{{$value->imagem}}" alt="{{$value->nome}}">
                </div>
                <div class="col-lg-7 col-md-7">
                    <h3>{{$value->nome}}</h3>
                    <p>{{$value->descricao}}</p>
                </div>
            </div>
        </a>
        <hr>
    @endforeach

@endsection
