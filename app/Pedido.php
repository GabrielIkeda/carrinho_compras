<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = [
        'usuarios_id'
    ];

    public static function todos_pedidos()
    {
        $pedidos = Pedido::with('usuario')->with('produto')->with('pedido_produto')->paginate(8);
        return $pedidos;
    }

    public function usuario()
    {
        return $this->belongsTo('App\Usuario', 'usuarios_id', 'id');
    }

    public function produto()
    {
        return $this->belongsToMany('App\Produto', 'pedido_produtos', 'pedidos_id', 'produtos_id');
    }

    public function pedido_produto()
    {
        return $this->hasMany('App\Pedido_Produto', 'pedidos_id', 'id');
    }
}
