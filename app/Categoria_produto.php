<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Categoria;
use App\Produto;

class Categoria_produto extends Model
{
    protected $table = 'categoria_produtos';
    protected $fillable = [
        'categoria_id', 'produtos_id',
    ];
}
