<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido_Produto extends Model
{
    protected $table = 'pedido_produtos';
    protected $fillable = [
        'pedidos_id', 'produtos_id', 'quantidade'
    ];

    public function pedido()
    {
        return $this->belongsTo('App\Produto');
    }
}
