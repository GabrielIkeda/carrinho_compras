<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Produto;

class Caracteristica extends Model
{
    public function produto()
    {
        return $this->belongsToMany('App\Produto', 'caracteristica_produtos', 'caracteristicas_id', 'produtos_id');
    }

    public function caracteristica()
    {
        return $this->belongsToMany('App\Caracteristica', 'caracteristica_produtos', 'produtos_id', 'caracteristicas_id');
    }
}
