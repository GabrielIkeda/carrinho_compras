<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Usuario extends Model
{
    protected $fillable = [
        'nome', 'cpf', 'email', 'enderecos_id'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    public function pedido()
    {
        return $this->belongsTo('App\Pedido', 'pedidos_id');
    }

    public static function cpf_unico($request)
    {
        $rules = array('cpf' => 'unique:usuarios,cpf');

        $validator = Validator::make(['cpf' => $request->cpf], $rules);

        if ($validator->fails()) {
            $return = false;
        } else {
            $return = true;
        }

        return $return;
    }

    public static function email_unico($request)
    {
        $rules = array('email' => 'unique:usuarios,email');

        $validator = Validator::make(['email' => $request->email], $rules);

        if ($validator->fails()) {
            $return = false;
        } else {
            $return = true;
        }

        return $return;
    }
}
