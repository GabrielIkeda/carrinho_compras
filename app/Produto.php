<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;

use App\Caracteristica;
use App\Categoria_produto;
use App\Categoria;

class Produto extends Model
{
    protected $fillable = [
        'nome', 'descricao', 'imagem', 'preco',
    ];

    public static function exibicao_produtos()
    {
        $produtos = Produto::with('caracteristica')
                                        ->with('categoria')
                                        ->paginate(6);
        return $produtos;
    }

    public static function exibicao_produto($id)
    {
        if (!empty($id)) {
            $produto = Produto::where('id', '=', $id)
                                ->with('caracteristica')
                                ->with('caracteristica_produto')
                                ->with('categoria')
                                ->get()
                                ->first();
            return $produto;
        } else {
            return null;
        }
    }

    public static function pesquisa_produto($pesquisa)
    {
        $resultado = Produto::where('nome', 'LIKE', '%'.$pesquisa.'%')
                                ->orWhere('descricao', 'LIKE', '%'.$pesquisa.'%')
                                ->get();
        return $resultado;
    }

    public function categoria()
    {
        return $this->belongsToMany('App\Categoria', 'categoria_produtos', 'produtos_id', 'categorias_id');
    }

    public function caracteristica()
    {
        return $this->belongsToMany('App\Caracteristica', 'caracteristica_produtos', 'produtos_id', 'caracteristicas_id');
    }

    public function caracteristica_produto()
    {
        return $this->hasMany('App\Caracteristica_Produto', 'produtos_id', 'id');
    }

    public function pedido()
    {
        return $this->hasMany('App\Pedido', 'pedido_produtos', 'pedidos_id', 'produtos_id');
    }
}
