<?php

namespace App;

class Carrinho
{
    public $itens;
    public $total_qtd = 0;
    public $total_preco = 0;

    public function __construct($carrinho_antigo)
    {
        if ($carrinho_antigo) {
            $this->itens = $carrinho_antigo->itens;
            $this->total_qtd = $carrinho_antigo->total_qtd;
            foreach ($carrinho_antigo->itens as $key => $value) {
                $this->total_preco += $value['preco'];
            }
        }
    }

    public function adicionar($item, $id, $quantidade)
    {
        $armazena_item = ['qtd' => 0, 'preco' => $item->preco, 'item' => $item];
        if ($this->itens) {
            if (array_key_exists($id, $this->itens)) {
                $armazena_item = $this->itens[$id];
            }
        }
        $armazena_item['qtd'] += $quantidade;
        $armazena_item['preco'] = $item->preco * $armazena_item['qtd'];
        $this->itens[$id] = $armazena_item;
        $this->total_qtd += $quantidade;
        $this->total_preco += $item->preco;
    }

    public static function remover($carrinho, $id)
    {
        $carrinho->total_qtd -= $carrinho->itens[$id]['qtd'];
        $carrinho->total_preco -= $carrinho->itens[$id]['preco'];
        unset($carrinho->itens[$id]);
    }

    public static function atualiza($carrinho, $id, $qtd)
    {
        $carrinho->itens[$id]['qtd'] = $qtd;
        $carrinho->itens[$id]['preco'] = $carrinho->itens[$id]['item']->preco * $qtd;

        $carrinho->total_qtd = 0;
        $carrinho->total_preco = 0;

        foreach ($carrinho->itens as $key => $value) {
            $carrinho->total_qtd += $value['qtd'];
            $carrinho->total_preco += $value['preco'];
        }
    }
}
