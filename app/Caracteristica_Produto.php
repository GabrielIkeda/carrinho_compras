<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caracteristica_Produto extends Model
{
    protected $table = 'caracteristica_produtos';
    protected $fillable = [
        'valor', 'caracteristicas_id', 'produtos_id'
    ];

    public function caracteristica()
    {
        return $this->belongsTo('App\Produto');
    }
}
