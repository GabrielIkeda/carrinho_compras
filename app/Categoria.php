<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categoria_produto;
use App\Produto;

class Categoria extends Model
{
    protected $fillable = [
        'nome',
    ];

    public static function exibicao_categorias()
    {
        $categorias = Categoria::all();
        return $categorias;
    }

    public static function pesquisa_categoria($pesquisa)
    {
        $resultado = Categoria::whereIn('nome', $pesquisa)
                                ->with('produto')
                                ->paginate(6);
        return $resultado;
    }

    public function produto()
    {
        return $this->belongsToMany('App\Produto', 'categoria_produtos', 'categorias_id', 'produtos_id');
    }
}
