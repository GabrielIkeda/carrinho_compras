<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use App\Produto;
use App\Carrinho;

class ProdutoController extends Controller
{
    public function index($id = null)
    {
        $produto = Produto::exibicao_produto($id);

        $caracteristicas = array();

        foreach ($produto->caracteristica as $key => $value) {
            foreach ($produto->caracteristica_produto as $k => $v) {
                $caracteristicas[$value->nome] = $v->valor;
            }
        }

        if ($produto != null) {

            if (Cache::has('carrinho')) {
                $carrinho = Cache::get('carrinho');
                if (isset($carrinho->itens[$id])) {
                    if ($carrinho->itens[$id]['qtd'] == 10) {
                        $limite = 10;
                        return view('produto', compact('produto', 'limite', 'caracteristicas'));
                    } else {
                        $limite = 10 - $carrinho->itens[$id]['qtd'];
                        return view('produto', compact('produto', 'limite', 'caracteristicas'));
                    }
                } else {
                    return view('produto', compact('produto', 'caracteristicas'));
                }
            } else {
                return view('produto', compact('produto', 'caracteristicas'));
            }
        } else {
            redirect()->route('index');
        }
    }

    public function adicionar_produto($id, Request $request)
    {
        $produto = Produto::find($id);
        $carrinho_antigo = Cache::has('carrinho') ? Cache::get('carrinho') : null;
        $carrinho = new Carrinho($carrinho_antigo);
        $carrinho->adicionar($produto, $produto->id, $request->quantidade);
        Cache::put('carrinho', $carrinho, 20);
        return redirect()->route('produto', ['id' => $id]);
    }

    public function pesquisa_produto(Request $request)
    {
        $resultado = Produto::pesquisa_produto($request->pesquisa);
        $pesquisa = $request->pesquisa;
        return view('pesquisa', compact('resultado', 'pesquisa'));
    }
}
