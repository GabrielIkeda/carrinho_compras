<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pedido;
use App\Pedido_Produto;

class PedidoController extends Controller
{
    public function index()
    {
        $pedidos = Pedido::todos_pedidos();

        foreach ($pedidos as $key => $value) {
            $t = 0;
            foreach ($value->produto as $k => $v) {
                $t += $v->preco * $value->pedido_produto[$k]->quantidade;
            }
            $total[$key] = $t;
        }

        return view('pedido', compact('pedidos', 'total'));
    }
}
