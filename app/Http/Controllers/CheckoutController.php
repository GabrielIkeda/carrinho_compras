<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use App\Usuario;
use App\Endereco;
use App\Pedido;
use App\Pedido_Produto;

class CheckoutController extends Controller
{
    public function index()
    {
        return view('checkout');
    }

    public function registrar(Request $request)
    {
        $carrinho = Cache::get('carrinho');

        $cpf_unico = Usuario::cpf_unico($request);
        $email_unico = Usuario::email_unico($request);

        if ($cpf_unico && $email_unico) {
            $endereco = Endereco::create([
                'logradouro' => $request->logradouro,
                'numero' => $request->numero,
                'cep' => $request->cep,
                'bairro' => $request->bairro,
                'cidade' => $request->cidade,
                'estado' => $request->estado,
            ]);

            $usuario = Usuario::create([
                'nome' => $request->nome,
                'cpf' => $request->cpf,
                'email' => $request->email,
                'enderecos_id' => $endereco->id,
            ]);

            $pedido = Pedido::create([
                'usuarios_id' => $usuario->id,
            ]);

            foreach ($carrinho->itens as $key => $value) {
                $pedido_produto[$key] = Pedido_Produto::create([
                    'pedidos_id' => $pedido->id,
                    'produtos_id' => $key,
                    'quantidade' => $value['qtd'],
                ]);
            }

            Cache::forget('carrinho');

            $notification = array(
                'message' => 'Pedido feito com sucesso!',
                'alert-type' => 'success'
            );
            session()->put('notification',$notification);

            return redirect('/');
        } else {
            $notification = array(
                'message' => 'Erro: CPF ou E-mail duplicado!',
                'alert-type' => 'error'
            );
            session()->put('notification',$notification);
            return redirect('/checkout');
        }


    }
}
