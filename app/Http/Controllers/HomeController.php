<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Produto;
use App\Categoria;

class HomeController extends Controller
{
    public function index()
    {
        $produtos = Produto::exibicao_produtos();
        $categorias = Categoria::exibicao_categorias();

        return view('home', compact('produtos', 'categorias'));
    }

    public function categoria(Request $request)
    {
        if (empty($request->except('_token'))) {
            return redirect('/');
        }
        
        $produtos = Categoria::pesquisa_categoria($request->except('_token'));
        $categorias = Categoria::exibicao_categorias();

        $filtro = array();

        foreach ($categorias as $key => $value) {
            foreach ($produtos as $k => $v) {
                if ($value->nome == $v->nome) {
                    $filtro[$key] = $v->nome;
                }
            }
        }

        $produtos_exibicao = array();

        foreach ($produtos as $key => $value) {
            foreach ($value->produto as $k => $v) {
                $produtos_exibicao[$v->id] = $v;
            }
        }

        // dd($produtos, $produtos_exibicao);

        return view('home', compact('produtos_exibicao', 'categorias', 'filtro', 'produtos'));
    }
}
