<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use App\Carrinho;

class CarrinhoController extends Controller
{
    public function index()
    {
        if (!Cache::has('carrinho')) {
            return view('carrinho_compras');
        }
        $carrinho_antigo = Cache::get('carrinho');
        $carrinho = new Carrinho($carrinho_antigo);
        return view('carrinho_compras', ['produtos' => $carrinho->itens, 'total_preco' => $carrinho->total_preco]);
    }

    public function remover_produto($id)
    {
        if (Cache::has('carrinho')) {
            $carrinho = Cache::get('carrinho');
            Carrinho::remover($carrinho, $id);
            Cache::put('carrinho', $carrinho, 20);
            return redirect()->route('carrinho_compras', ['produtos' => $carrinho->itens, 'total_preco' => $carrinho->total_preco]);
        }
    }

    public function atualiza_qtd($id, Request $request)
    {
        if (Cache::has('carrinho')) {
            $carrinho = Cache::get('carrinho');
            Carrinho::atualiza($carrinho, $id, $request->quantidade);
            Cache::put('carrinho', $carrinho, 20);
            return redirect()->route('carrinho_compras', ['produtos' => $carrinho->itens, 'total_preco' => $carrinho->total_preco]);
        }
    }
}
